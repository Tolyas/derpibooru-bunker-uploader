abs,пресс
adagio dazzle,Адажио
alcohol,алкоголь
allie way,Алли Вэй
aloe,Алоэ
amalthea,Амальтея
anal,анал
animated,анимированный
angry,сердитый
anthro,aнтро
apple bloom,Эппл Блум
apple bumpkin,Эппл Бампкин
applejack,Эпплджек
aria blaze,Ария
ass,попка
babs seed,Бэбс Сид
bat pony,бэт-пони
bdsm,<strong>BDSM</strong>
berry punch,Берри Панч
big breasts,большая грудь
huge breasts,огромная грудь
hyper breasts,гиперсиськи
impossibly large breasts,невозможно большая грудь
big macintosh,Биг Мак
blaze,Блейз
blossomforth,Блоссомфорт
blushing,румянец
body pillow,дакимакура
bon bon,Бон Бон
bondage,<strong>связывание</strong>
bottomless,боттомлесс
bow,бантик
braeburn,Брейберн
breast milk,грудное молоко
bukkake,буккакэ
bulk biceps,Балк Бицепс
bunny suit,костюм кролика
button mash,Баттон Мэш
cameltoe,верблюжья лапка
caramel,Карамель
cheerilee,Чирили
cheese sandwich,Чиз Сэндвич
cherry jubilee,Черри Джубили
citrus blush,Цитрус Блаш
cleavage,декольте
close-up,близко
clothes,одежда
cloudchaser,Клаудчейзер
coco pommel,Коко Поммель
collar,ошейник
coloratura,Колоратура
comic,комикс
condom,презерватив
copper top,Коппер Топ
crossdressing,кроссдрессинг
crotchboobs,вымечко
crying,плач
crystal empire,Кристальная Империя
cum,сперма
cunnilingus,куннилингус
curvy,фигуристая
daisy,Дейзи
dakimakura pose,на дакимакуру
daring do,Дэринг Ду
derpy hooves,Дерпи
dialogue,диалог
diamond tiara,Даймонд Тиара
dinky hooves,Динки
discord,Дискорд
doctor whooves,Доктор Хувс
drizzle,Дриззл
eared humanization,с пони-ушками
epona,Эпона
equestria girls,EГ
facial,на лицо
fat,полнота
femdom,<strong>фемдом</strong>
fingering,фингеринг
fleetfoot,Флитфут
fleur-de-lis,Флер-де-лис
flitter,Флиттер
fluttershy,Флаттершай
foalcon,фоалкон
foot fetish,фут фетиш
futa,фута
gabby,Габби
gag,кляп
gay,геи
gilda,Гильда
glasses,очки
grimdark,<strong>мрачно</strong>
handjob,мастурбация партнеру
hoers,лошоть
horned humanization,с рогом
humanized,xуманизация
human on pony action,человек и пони
incest,инцест
indigo zap,Индиго Зап
irl human,irl человек
jet set,Джет Сет
king sombra,Сомбра
kissing,поцелуй
lactation,выделение молока
lavender essence,Лэвендер Эссенс
leash,поводок
lemon hearts,Лемон Хартс
lemony gem,Лемони Джем
lemon zest,Лемон Цест
leotard,трико
lesbian,лесби
lightning dust,Лайтнинг Даст
lily,Лили
limestone pie,Лаймстоун Пай
lingerie,женское белье
little strongheart,Литтл Стронгхарт
lotus blossom,Лотос
lucy packard,Люси Пакард
lyra heartstrings,Лира
macro,макро
maid,горничная
marble pie,Марбл Пай
masturbation,мастурбация
maud pie,Мод Пай
mayor mare,Мэр
megan williams,Меган Уильямс
micro,микро
milf,милф
minuette,Менуэт
monochrome,одноцветный
moondancer,Мундэнсер
ms. harshwhinny,Харшвинни
muscles,мускулы
night glider,Найт Глайдер
nightie,ночнушка
night light,Найт Лайт
nightmare moon,Найтмер Мун
nightmare rarity,Найтмер Рэрити
nipples,соски
nudity,нагота
nurse redheart,Рэдхарт
octavia melody,Октавия
oc,ОС
oral,орал
orgy,оргия
panties,трусики
parasol,Парасоль
peach fuzz,Пич Фаз
perky prep,Перки Преп
pillow,подушка
pinkie pie,Пинки
pipsqueak,Пипсквик
plantigrade anthro,не копытное
plot,плот
plump,полнота
ponified,понификация
presenting,презентация
prim hemline,Прим Хемлайн
prim posy,Прим Пози
prince blueblood,Блюблад
princess cadance,Кейденс
princess celestia,Селестия
princess ember,Эмбер
princess flurry heart,Фларри Харт
princess luna,Луна
pyjamas,пижама
queen chrysalis,Кризалис
rainbow dash,Рэйнбоу
raindrops,Рэйндропс
rape,<strong>изнасилование</strong>
rarity,Рэрити
ribbon,ленточка
riding crop,хлыст
roseluck,Роузлак
royal guard,стражник
rule 63,правило 63
rumble,Рамбл
saffron masala,Саффрон Масала
sapphire shores,Сапфайр Шорс
sassaflash,Сэссафлеш
scootaloo,Скуталу
semi-grimdark,<strong>мрачновато</strong>
serena,Серена
sex toy,секс-игрушка
sex,секс
shadowbolt,шедоуболт
shining armor,Шайнинг Армор
silver spoon,Сильвер Спун
siren,сирена
soarin',Соарин
socks,носочки
sonata dusk,Соната
sour sweet,Соур Свит
spanking,шлёпание
spike,Спайк
spitfire,Спитфайр
spoiled rich,Спойлед Рич
spring forward,Спринг Форвард
starlight glimmer,Старлайт
stockings,чулочки
straight,гетеро
strapon,страпон
sugar belle,Шугар Белль
sugarcoat,Шугаркоут
sunburst,Санбёрст
sunflower (pony),Санфлауэер
sunny flare,Санни Флейр
sunset shimmer,Сансет
suri polomare,Сури Поломейр
surprised,удивление
surprise,Сюрпрайз
sweetie belle,Свити Белль
swimsuit,купальник
tail bow,бантик на хвосте
tailed humanization,с хвостиком
tara strong,Тара Стронг
tentacles,тентакли
the ass was fat,зад был широк
tickling,щекотка
topless,топлесс
tracer,Трейсер
tree hugger,Три Хаггер
tribadism,трибадизм
trixie,Трикси
twilight sparkle,Твайлайт
twilight velvet,Твайлайт Вельвет
twinkleshine,Твинклшайн
twist,Твист
underwear,белье
unguligrade anthro,копытное
upper crust,Аппер Краст
vaginal secretions,вагинальная секреция
vector,вектор
vinyl scratch,Винил
vore,<strong>пожирание</strong>
vulgar,вульгарно
watersports,<strong>уринация</strong>
wide hips,широкие бедра
winged humanization,с крыльями
wrestling,борьба
zecora,Зекора
zephyr breeze,Зефир Бриз
zoe orimoto,Зоэ Оримото
high heel,Хай Хил
mane-iac,Мэйниак
school uniform,школьная форма
candy mane,Кэнди Мэйн
katsuragi,Кацураги
amira,Амира
zapp,Разряд
deer,олень
vapor trail,Вейпор Трейл
flam,Флам
flim,Флим
hong meiling,Хун Мэйлин
jackie lynn thomas,Джеки Линн Томас
horo,Хоро
alarak,Аларак
q,Кью
sky stinger,Скай Стингер
sombra (overwatch),Сомбра (Overwatch)
grace manewitz,Грейс Мейнвитц
peachy plume,Пичи Плюм
raven,Рейвен
griffon,грифон
spider,паук
mei,Мэй
overwatch,Overwatch
quibble pants,Квиббл Пэнтс
boop,буп
paisley,Пейсли
double diamond,Дабл Даймонд
party favor,Парти Фейвор
zettai ryouiki,дзэттай-рёики
gloriosa daisy,Глориоза Дэйзи
shorts,шорты
ambiguous facial structure,неоднозначная структура лица
marigold heavenly nostrils,Меригольд Небесные Ноздри
classical unicorn,классический единорог
fili-second,Понисекунда
masked matter-horn,Маг Материи в Маске
mistress marevelous,Госпожа Великолепие
radiance,Сияющая
saddle rager,Сбруйный Гнев
majorette,Мажоретт
sweat,пот
shipping,шиппинг
smiling,улыбка
changeling,чейнджлинг
book,книга
amethyst star,Аметист Стар
wat,чоч
firefly,Файрфлай
plushie,плюшка
weapon,оружие
hat,шапка
scarf,шарф
cake,торт
prince of persia,принц персии
muffin,маффин
eevee,Иви
chest fluff,грудной пушок
sassy saddles,Сэсси Сэддлс
drool,слюни
saliva trail,нить слюны
spider-man,Человек-паук
mrs. shy,Миссис Шай
roskomnadzor,Роскомнадзор
pregnant,беременность
trixie's mom,мама Трикси
size difference,разница в размере
corset,корсет
garter,подвязка
nightgown,ночнушка
bowtie,бабочка
bestiality,зоофилия
commander hurricane,командир Ураган
private pansy,рядовой Пэнси
cinnamon chai,Синнамон Чай
hug,обнимашки
rainbowshine,Рэйнбоушайн
taralicious,Таралишес
seizure warning,<strong>предупреждение о мелькании</strong>
trenderhoof,Трендерхуф
tongue out,язычок
parasprite,параспрайт
wet,мокрый
mistletoe,омела
freckles,веснушки
pajamas,пижама
sleeping,сон
sunset,закат
tea,чай
one finger selfie challenge,челлендж "Одним пальцем"
your character here,заготовка "ваш персонаж здесь"
sweater,свитер
banana,банан
prostitution,проституция
anatomically correct,анатомически верно
cloud kicker,Клауд Кикер
virgin killer sweater,свитер-убийца девственников
hoodie,толстовка
twilight sparkle (alicorn),твайликорн
apple cobbler,Эппл Кобблер
sweetcream scoops,Свиткрим Скупс
3d,3D
flash sentry,Флеш Сентри
fancypants,Фэнсипэнтс
svengallop,Свенгалоп
granny smith,Бабуля Смит
younger,моложе
cauldron bubbles,Колдрон Бабблс
femboy,фембой
disembodied penis,пенис без тела
sketch,набросок
featherweight,Фезервейт
cat ears,кошачьи ушки
fangs,клыки
cucumber,огурец
clear skies,Клир Скайз
juniper montage,Джунипер Монтаж
older,старше
sea swirl,Си Свирл
diamond mint,Даймонд Минт
chibi,чиби
flower,цветок
superman,Супермен
misty fly,Мисти Флай
calamity mane,Каламити Мейн
cauldron bubbles,Колдрон Бабблс
marian,Мэриэн
nosey news,Ноузи Ньюс
blood,кровь
nosebleed,кровь из носа
photo finish,Фото Финиш
cup cake,Кап Кейк
mermaid,русалка
eris,Эрида
gummy,Гамми
unconvincing armor,неубедительная броня
chainmail bikini,бронелифчик
pinkamena diane pie,Пинкамина
sea pony,морские поньки&#769;
merpony,пони-русалки
balloon,воздушный шарик
philomena,Филомина
laughing,смех
open mouth,рот открыт
thorax,Торакс
uncanny valley,зловещая долина
masquerade,Маскарад
woona,Вуна
alternate design,альтернативный дизайн
starlight,Старлайт
sweet leaf,Свит Лиф
gina,Джина
heterochromia,гетерохромия
mare do well,Мэйр Ду Велл
centaur,кентавр
kirin,кирин
daybreaker,Дэйбрейкер
paprika paca,Паприка Пака
spiderpony,паукопони
windy whistles,Винди Вистлс
fusion,слияние
star swirl the bearded,Старсвирл Бородатый
blank flank,пустобокий
dragon lord torch,Владыка Драконов Торч
bow hothoof,Боу Хотхуф
ovipositor,яйцеклад
cartographer's cap,картографическая шляпа
sword,меч
measuring tape,измерительная лента
kite,воздушный змей
midnight sparkle,Миднайт Спаркл
lily lace,Лили Лейс
inky rose,Инки Роуз
starstreak,Старстрик
moonlight raven,Мунлайт Рэйвен
elephant,слон
hard hat,каска
photo finish,Фото Финиш
pear butter,Пэар Баттер
bright mac,Брайт Мак
selfcest,селфцест
nightmare star,Найтмер Стар
raspberry vinaigrette,Распберри Винигрет
genie,джинн
adult,взрослый
tempest shadow,Темпест Шадоу
princess molestia,Молестия
flower wishes,Флауэр Вишес
bed,кровать
belly dancer,танцор живота
tail wrap,обмотка хвоста
songbird serenade,Сонгбёрд Серенад
jasmine leaf,Жасмин Лиф
steven magnet,Стивен Магнет
dear darling,Диа Дарлинг
feather bangs,Фезер Бэнгс
fond feather,Фонд Фезер
swoon song,Свун Сонг
stormy flare,Шторми Флейр
ice cream,мороженка
cloudy quartz,Клауди Кварц
cookie crumbles,Куки Крамблс
toola roola,Тула Рула
coconut cream,Коконат Крим
rainbow stars,Рейнбоу Старс
alicorn,аликорн
alicornified,аликорнизация
mistmane,Мистмэйн
kettle corn,Кеттл Корн
captain celaeno,капитан Сэлано
cuphead,"Cuphead"
princess skystar,Скайстар
glitter drops,Глиттер Дропс
spring rain,Спринг Рейн
scribble dee,Скриббл Ди
pinny lane,Пинни Лейн
sphinx (character),Сфинкс
somnambula,Сомнамбула
gaea everfree,Гайя Эверфри
snail pony,улиткопони
cheerleader,чирлидер
rain,дождь
queen novo,королева Ново
grand pear,Гранд Пэар
greta,Грета
sphinx,сфинкс
fuchsia blush,Фуксия Блаш
lavender lace,Лавендер Лейс
melon mint,Мелон Минт
crystal lullaby,Кристал Лалэбай
frosty orange,Фрости Оранж
garden grove,Гарден Гроув
pound cake,Паунд Кейк
pumpkin cake,Пампкин Кейк
hondo flanks,Хондо Фланкс
leak,<strong>слив</strong>
diwata aino,Дивата Айно
capper,Каппер
ugandan knuckles,угандийские наклзы
original species,оригинальный вид
zephyr,Зефир
biting,кусь
ear bite,ухокусь
unshorn fetlocks,нестриженные щетки
human coloration,натуральный цвет кожи
lily longsocks,Лили Лонгсокс
hippogriff,гиппогриф
wallflower blush,Уоллфлауэр Блаш
timber spruce,Тимбер Спрус
grubber,Граббер
boysenberry,Бойзенберри
cosplay,косплей
silverstream,Сильверстрим
smolder,Смолдер
gallus,Галлус
ocellus,Оцеллия
sandbar,Сэндбар
yona,Йона
jack pot,Джек Пот
big bucks,Биг Бакс
snails,Снейлз
snips,Снипс
raspberry beret,Распберри Берет
meadowbrook,Мидоубрук
pony of shadows,Пони Теней
radiant hope,Рэдиент Хоуп
wing ears,ушки-крылья
stellar flare,Стеллар Флейр
firelight,Файрлайт
thunderlane,Тандерлейн
sweet biscuit,Свит Бисквит
scar,шрам
bedroom eyes,томный взгляд
cozy glow,Кози Глоу
shutter bug,Шаттер Баг
zippoorwhill,Зиппурвилл
mercy,Мёрси
widowmaker,Вдова
clone twilight,клон Твайлайт
clone,клон
detroit: become human,Детройт: Стать человеком
vignette valencia,Виньет Валенсия
chancellor puddinghead,канцлер Пудингхед
princess platinum,принцесса Платинум
clover the clever,Кловер Мудрая
smart cookie,Смарт Куки
geralt of rivia,Геральт из Ривии
nightshade,Найтшейд
kibitz,Кибитц
professor inkwell,профессор Инквелл
pantyhose,колготки
ahegao,ахегао
strawberry sunrise,Строуберри Санрайз
capper dapperpaws,Каппер Дапперпоус
best pony,лучшая пони
barb,Барб
bowsette,Боузетта
autumn blaze,Отум Блейз
deltarune,&Delta;rune
ralsei,Ралсей
pharynx,Фаринкс
bikini,бикини
gloves,перчатки
bathtub,ванна
cute,милашно
cinder glow,Синдер Глоу
open-chest hoodie,худи открытой груди
lamia,ламия
penis,пенис
big penis,большой пенис
hyper penis,гиперпенис
impossibly large penis,невозможно большой пенис
small penis,маленький пенис
alice the reindeer,Олень Элис
aurora the reindeer,Олень Аврора
bori the reindeer,Олень Бори
principal abacus cinch,директор Абакус Синч
clear sky (character),Клир Скай
wind sprint,Винд Спирит
the great seedling,Великий Урожайник
tree of harmony,Дерево Гармонии
cheongsam,ципао
edith (cockatrice),Эдит
aunt holiday,тётя Холидей
auntie lofty,тётушка Лофти
mane allgood,Мейн Оллгуд
snap shutter,Снеп Шаттер
kiwi lollipop,Киви Лоллипоп
supernova zap,Супернова Зап
anime,аниме
torque wrench,Торк Ренч
lighthoof,Лайтхуф
shimmy shake,Шимми Шейк
orange sherbette,Оранж Щербет
luster dawn,Ластер Дон
amputee,ампутация
kerfuffle,Кирфаффл
angel bunny,Энджел (кролик)
desert flower,Дезерт Флауер
male,парень
lord tirek,Тирек
izzy moonbow,Иззи Мунбоу
pipp,Пипп
sunny starscout,Санни Старскаут
hitch trailblazer,Хитч Трейлблейзер
tail,<strong>хвост</strong>
